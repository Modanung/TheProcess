# The Process

A vertical space shooter, written in Processing, which is used as an example at the [Dojo](https://github.com/richelbilderbeek/Dojo).

[![Travis CI logo](TravisCI.png)](https://travis-ci.org)

[![Build Status](https://travis-ci.org/Modanung/TheProcess.svg?branch=master)](https://travis-ci.org/Modanung/TheProcess)
=======
### Screenshots

![Screenshot of TheProcess v1.1](Screenshots/TheProcess_1_2.png)
![Screenshot of TheProcess v1.1](Screenshots/TheProcess_1_1.png)