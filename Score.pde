class Score {
  int score;
  PFont font = createFont("WhiteRabbitReloaded.ttf", 64);

  Score()
  {
    reset();
  }

  void display()
  {
    fill(0, 255, 0, 128);
    stroke(0, 255, 0);
    textFont(font);
    text(score, playerLives() ? width / 5 : height / 40, height / 18);
  }

  void reset()
  {
    setScore(0);
  }

  void setScore(int points)
  {
    score = points;
  }

  void addScore(int points)
  {
    setScore(score + points);
  }
}

class Journey {
  float totalDistance;
  float traveled = 0.0;
  float reward = 0.0;

  Journey(float distance)
  {
    totalDistance = distance;
  }

  void update()
  {
    if (playerLives()) {
      float travelTick = player.travelSpeed() * tick * 0.01;
      traveled += travelTick;
      reward += 2.3 * travelTick * travelTick;
      if (reward > 1.0f) {
        player.score.addScore(1);
        reward -= 1.0f;
      }
    }
  }

  void display()
  {
    float y = height - height * traveled / totalDistance;

    fill(0, 255, 0);
    noStroke();

    beginShape();
    vertex(width - 24, y);
    vertex(width - 32, y - 23);
    vertex(width, y);
    vertex(width - 32, y + 23);
    endShape();

    beginShape();
    vertex(24, y);
    vertex(32, y - 23);
    vertex(0, y);
    vertex(32, y + 23);
    endShape();

    strokeWeight(2);
    int lines = 25;
    for (int i = 0; i < lines; ++i) {
      y = height - i * height / lines;
      stroke(0, 255, 0);
      noFill();
      line(i % 5 == 0 ? width - 24 : width - 8, y, width, y);
      line(0, y, i % 5 == 0 ? 24 : 8, y);
    }
  }
}